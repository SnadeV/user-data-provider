package com.empik.userdataprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class UserDataProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserDataProviderApplication.class, args);
    }

}
