package com.empik.userdataprovider.application.rest;

import com.empik.userdataprovider.application.response.UserResponse;
import com.empik.userdataprovider.domain.service.DomainUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/users")
public class UserController {

    private final DomainUserService userService;

    @GetMapping(value = "/{login}", produces = APPLICATION_JSON_VALUE)
    UserResponse readUser(@PathVariable("login") String login) {
        log.debug("readUser() - login: {}", login);

        return userService.readUser(login);
    }
}