package com.empik.userdataprovider.application.rest;

import com.empik.userdataprovider.domain.client.exception.GitHubUserNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerAdvice {

    @Getter
    @AllArgsConstructor
    private static class ExceptionDetails {
        private final String message;
    }

    @ExceptionHandler(value = {GitHubUserNotFoundException.class})
    ResponseEntity<ExceptionDetails> handleNotFoundException(Exception ex) {
        return getResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<ExceptionDetails> getResponse(String messageBody, HttpStatus status) {
        final ExceptionDetails body = new ExceptionDetails(messageBody);
        return new ResponseEntity<>(body, status);
    }

}