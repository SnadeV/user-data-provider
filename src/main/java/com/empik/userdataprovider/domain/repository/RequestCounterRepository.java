package com.empik.userdataprovider.domain.repository;

import com.empik.userdataprovider.domain.RequestCounter;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface RequestCounterRepository extends CrudRepository<RequestCounter, String> {

    // delegating update to database to avoid concurrency issues
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE RequestCounter rc set rc.requestCount = rc.requestCount + 1 WHERE rc.login = :login")
    int incrementCounterByLogin(@Param("login") String login);

}