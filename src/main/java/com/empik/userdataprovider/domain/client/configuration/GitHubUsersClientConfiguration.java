package com.empik.userdataprovider.domain.client.configuration;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class GitHubUsersClientConfiguration {

    @Bean
    public ErrorDecoder errorDecoder() {
        return new GitHubUsersClientErrorDecoder();
    }
}