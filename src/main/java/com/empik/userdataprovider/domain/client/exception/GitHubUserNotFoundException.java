package com.empik.userdataprovider.domain.client.exception;

public class GitHubUserNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 8959978532653967522L;

    public GitHubUserNotFoundException() {
        super("GitHub user not found");
    }

}
