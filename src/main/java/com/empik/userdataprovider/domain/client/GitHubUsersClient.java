package com.empik.userdataprovider.domain.client;

import com.empik.userdataprovider.domain.GitHubUser;
import com.empik.userdataprovider.domain.client.configuration.GitHubUsersClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "github", url = "https://api.github.com/users", configuration = GitHubUsersClientConfiguration.class )
public interface GitHubUsersClient {

    @RequestMapping(method = RequestMethod.GET, value = "/{login}", produces = "application/json")
    GitHubUser getUserByLogin(@PathVariable("login") String login);
}