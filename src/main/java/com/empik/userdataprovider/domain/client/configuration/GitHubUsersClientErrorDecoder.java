package com.empik.userdataprovider.domain.client.configuration;

import com.empik.userdataprovider.domain.client.exception.GitHubUserNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.http.HttpStatus;


public class GitHubUsersClientErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() == HttpStatus.NOT_FOUND.value()) {
            return new GitHubUserNotFoundException();
        }

        return new Exception(response.reason());
    }
}