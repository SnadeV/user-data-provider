package com.empik.userdataprovider.domain;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "login")
public class RequestCounter {

    @Id
    @Column(nullable = false)
    private String login;

    @NotNull
    @Column(nullable = false)
    private int requestCount;

    public static RequestCounter of(String login) {
        return new RequestCounter(login, 1);
    }

}
