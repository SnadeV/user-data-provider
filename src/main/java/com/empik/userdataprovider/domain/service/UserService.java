package com.empik.userdataprovider.domain.service;

import com.empik.userdataprovider.application.response.UserResponse;

public interface UserService {
    UserResponse readUser(String login);
}
