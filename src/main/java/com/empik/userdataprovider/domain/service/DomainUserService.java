package com.empik.userdataprovider.domain.service;

import com.empik.userdataprovider.application.response.UserResponse;
import com.empik.userdataprovider.domain.RequestCounter;
import com.empik.userdataprovider.domain.client.GitHubUsersClient;
import com.empik.userdataprovider.domain.repository.RequestCounterRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class DomainUserService implements UserService {

    private final GitHubUsersClient gitHubUsersClient;
    private final RequestCounterRepository requestCounterRepository;

    @Override
    @Transactional
    public UserResponse readUser(String login) {
        log.debug("readUser() - login: {}", login);

        final UserResponse userResponse = gitHubUsersClient.getUserByLogin(login).toUserResponse();
        createOrIncrementRequestCounterByLogin(login);

        return userResponse;
    }

    private void createOrIncrementRequestCounterByLogin(String login) {
        log.debug("createOrIncrementRequestCounterByLogin() - login: {}", login);

        final int updatedRows = requestCounterRepository.incrementCounterByLogin(login);
        if(updatedRows == 0) {
            createRequestCounterByLogin(login);
        }
    }

    private void createRequestCounterByLogin(String login) {
        log.debug("createRequestCounterByLogin() - login: {}", login);

        final RequestCounter requestCounter = RequestCounter.of(login);
        requestCounterRepository.save(requestCounter);
    }
}
