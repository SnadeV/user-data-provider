package com.empik.userdataprovider.domain;

import com.empik.userdataprovider.application.response.UserResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

@Slf4j
@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class GitHubUser {
    private String login;
    private int id;
    private String avatarUrl;
    private String type;
    private String name;
    private BigDecimal followers;
    private BigDecimal publicRepos;
    private LocalDateTime createdAt;

    public UserResponse toUserResponse() {
        final BigDecimal calculations = doCalculations();
        return new UserResponse(id, login, avatarUrl, type, name, createdAt, calculations);
    }

    private BigDecimal doCalculations() {
        log.debug("doCalculations() - followers: {}, publicRepos: {}", followers, publicRepos);

        // returns null if the calculations are not possible (dividing by zero)
        if(followers.equals(BigDecimal.ZERO)) {
            return null;
        }

        final BigDecimal sumPart = BigDecimal.valueOf(2).add(publicRepos);
        final BigDecimal multiplicationPart = followers.multiply(sumPart);
        return BigDecimal.valueOf(6).divide(multiplicationPart, 10, RoundingMode.HALF_UP);
    }
}