package com.empik.userdataprovider.infrastructure.docs;

import com.empik.userdataprovider.application.response.UserResponse;
import com.empik.userdataprovider.domain.GitHubUser;
import com.empik.userdataprovider.domain.service.DomainUserService;
import com.empik.userdataprovider.domain.util.GitHubUserProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest
public class UserControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private DomainUserService service;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .build();
    }

    @Test
    public void shouldReturnUserData() throws Exception {
        final GitHubUser user = GitHubUserProvider.getReadGitHubUser();
        final UserResponse userResponse = user.toUserResponse();

        given(service.readUser(user.getLogin())).willReturn(userResponse);

        this.mockMvc.perform(get("/users/{login}", "octocat"))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(document("user-read",
                        pathParameters(parameterWithName("login").description("The login of the user to read"))
                ));
    }
}
