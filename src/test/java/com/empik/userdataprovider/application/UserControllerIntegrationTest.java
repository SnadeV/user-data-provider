package com.empik.userdataprovider.application;

import com.empik.userdataprovider.application.response.UserResponse;
import com.empik.userdataprovider.application.rest.UserController;
import com.empik.userdataprovider.domain.GitHubUser;
import com.empik.userdataprovider.domain.client.exception.GitHubUserNotFoundException;
import com.empik.userdataprovider.domain.service.DomainUserService;
import com.empik.userdataprovider.domain.util.GitHubUserProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DomainUserService service;

    @Test
    public void shouldReturnUserDataJson() throws Exception {
        final GitHubUser user = GitHubUserProvider.getReadGitHubUser();
        final UserResponse userResponse = user.toUserResponse();

        given(service.readUser(user.getLogin())).willReturn(userResponse);

        mvc.perform(get("/users/{login}", user.getLogin())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(user.getId())))
                .andExpect(jsonPath("$.login", is(user.getLogin())));

        verify(service, times(1)).readUser(user.getLogin());
    }

    @Test
    public void shouldReturnStatusNotFoundIfUserDoesNotExist() throws Exception {
        final String notExistingUserLogin = "i_am_nobody";

        given(service.readUser(notExistingUserLogin)).willThrow(GitHubUserNotFoundException.class);

        mvc.perform(get("/users/{login}", notExistingUserLogin)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(service, times(1)).readUser(notExistingUserLogin);
    }

}