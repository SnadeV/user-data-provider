package com.empik.userdataprovider.domain.service;

import com.empik.userdataprovider.domain.util.GitHubUserProvider;
import com.empik.userdataprovider.application.response.UserResponse;
import com.empik.userdataprovider.domain.GitHubUser;
import com.empik.userdataprovider.domain.RequestCounter;
import com.empik.userdataprovider.domain.client.GitHubUsersClient;
import com.empik.userdataprovider.domain.client.exception.GitHubUserNotFoundException;
import com.empik.userdataprovider.domain.repository.RequestCounterRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DomainUserServiceUnitTest {

    private GitHubUsersClient gitHubUsersClient;
    private RequestCounterRepository requestCounterRepository;
    private DomainUserService tested;

    @BeforeEach
    void setUp() {
        gitHubUsersClient = mock(GitHubUsersClient.class);
        requestCounterRepository = mock(RequestCounterRepository.class);
        tested = new DomainUserService(gitHubUsersClient, requestCounterRepository);
    }

    @Test
    void shouldReturnUserDataAndThenSaveRequestCounter() {
        final GitHubUser gitHubUser = spy(GitHubUserProvider.getReadGitHubUser());

        when(gitHubUsersClient.getUserByLogin(gitHubUser.getLogin())).thenReturn(gitHubUser);

        final UserResponse userResponse = tested.readUser(gitHubUser.getLogin());

        verify(requestCounterRepository).save(any(RequestCounter.class));
        verify(requestCounterRepository).incrementCounterByLogin(any(String.class));
        assertNotNull(userResponse.getLogin());
    }

    @Test
    void shouldThrowGitHubUserNotFoundExceptionIfUserDoesNotExist() {
        final String notExistingUserLogin = "i_am_nobody";

        when(gitHubUsersClient.getUserByLogin(notExistingUserLogin)).thenThrow(GitHubUserNotFoundException.class);

        final Supplier<UserResponse> supplier = () -> tested.readUser(notExistingUserLogin);

        verify(requestCounterRepository, times(0)).save(any(RequestCounter.class));
        verify(requestCounterRepository, times(0)).incrementCounterByLogin(any(String.class));
        assertThrows(GitHubUserNotFoundException.class, supplier::get);
    }

}
