package com.empik.userdataprovider.domain.repository;

import com.empik.userdataprovider.domain.RequestCounter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class RequestCounterRepositoryIntegrationTest {

    @Autowired
    private RequestCounterRepository tested;
    private RequestCounter requestCounter;

    @BeforeEach
    public void setUp() {
        requestCounter = RequestCounter.of("octocat");
        tested.save(requestCounter);
    }

    @Test
    public void shouldSaveRequestCounter() {
        Optional<RequestCounter> requestCounterOptional = tested.findById("octocat");

        assertTrue(requestCounterOptional.isPresent());
        assertEquals(requestCounterOptional.get(), requestCounter);
    }

    @Test
    public void shouldIncrementRequestCountIfCounterAlreadyExists() {
        final String requestCounterLogin = requestCounter.getLogin();
        tested.incrementCounterByLogin(requestCounterLogin);

        RequestCounter user = tested.findById(requestCounterLogin).get();

        assertEquals(2, user.getRequestCount());
    }

    @Test
    @Transactional(propagation = Propagation.NEVER)
    public void shouldIncrementingRequestCountBeThreadSafe() throws InterruptedException {
        final String userLogin = "octocat";
        final int numberOfThreads = 100;

        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);

        for (int i = 0; i < numberOfThreads; i++) {
            service.execute(() -> {
                tested.incrementCounterByLogin(userLogin);
                latch.countDown();
            });
        }

        latch.await();

        RequestCounter user = tested.findById(userLogin).get();

        assertEquals(user.getRequestCount(), numberOfThreads + 1);
    }
}
