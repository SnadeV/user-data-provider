package com.empik.userdataprovider.domain.util;

import com.empik.userdataprovider.domain.GitHubUser;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class GitHubUserProvider {
    public static GitHubUser getReadGitHubUser() {
        return new GitHubUser("octocat", 583231, "https://avatars.githubusercontent.com/u/583231?v=4", "User", "The Octocat", BigDecimal.valueOf(3904), BigDecimal.valueOf(8), LocalDateTime.of(2011, 1, 25, 18, 44, 36));
    }

    public static GitHubUser getGitHubUserWithZeroFollowers() {
        return new GitHubUser("lonely_guy", 21231, "https://avatars.githubusercontent.com/u/583231?v=4", "User", "The Lonely Guy", BigDecimal.valueOf(0), BigDecimal.valueOf(8), LocalDateTime.of(2000, 2, 11, 11, 12, 36));
    }

}