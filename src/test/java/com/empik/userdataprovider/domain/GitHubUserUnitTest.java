package com.empik.userdataprovider.domain;

import com.empik.userdataprovider.application.response.UserResponse;
import com.empik.userdataprovider.domain.util.GitHubUserProvider;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class GitHubUserUnitTest {

    @Test
    void shouldReturnCalculationsIfFollowersGreaterThanZero() {
        final GitHubUser gitHubUser = GitHubUserProvider.getReadGitHubUser();
        final UserResponse userResponse = gitHubUser.toUserResponse();

        assertNotNull(userResponse.getCalculations());
    }

    @Test
    void shouldReturnNullCalculationsIfZeroFollowers() {
        final GitHubUser gitHubUser = GitHubUserProvider.getGitHubUserWithZeroFollowers();
        final UserResponse userResponse = gitHubUser.toUserResponse();

        assertNull(userResponse.getCalculations());
    }

    @Test
    void shouldReturnValidCalculationsResult() {
        final GitHubUser gitHubUser = GitHubUserProvider.getReadGitHubUser();
        final UserResponse userResponse = gitHubUser.toUserResponse();

        assertEquals(userResponse.getCalculations(), BigDecimal.valueOf(0.0001536885));
    }
}
