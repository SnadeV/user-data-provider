# User Data Provider - recruitment task for Empik

#### Additional assumptions:
* the result of the calculation is rounded to ten decimal places
* when the calculation cannot be performed (dividing by zero when the user's followers count is equal zero), the result is null

#### Main tools used: 
* Java 11
* Spring Boot
* PostgreSQL
* Docker

## Getting started

#### Using `docker-compose`

In the terminal run the following command:
```console
$ docker-compose up
``` 

#### Using Maven

Check the configuration file `src/main/resources/application.yml` - if connection details are correct, run the command:

```console
$ mvn clean package
```

Finally, run the application using the following command:
```console
$ mvn spring-boot:run 
```

#### Using IntelliJ
First, configure PostgreSQL connection parameters in: `src/main/resources/application.yml`.

If the connection details are correct, just run the `UserDataProviderApplication.java` class.

#### Application will start at `localhost:8080`