FROM maven:latest as builder
WORKDIR /usr/src/user-data-provider
COPY pom.xml .
RUN mvn -B -f pom.xml -s /usr/share/maven/ref/settings-docker.xml dependency:resolve
COPY . .
RUN mvn -B -s /usr/share/maven/ref/settings-docker.xml package -DskipTests

FROM adoptopenjdk/openjdk11-openj9:alpine-slim
ARG JAR_FILE=/usr/src/user-data-provider/target/*.jar
COPY --from=builder ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]